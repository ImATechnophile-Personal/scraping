from bs4 import BeautifulSoup
from flask import Flask
import json
import requests
url=requests.get('https://www.spicinemas.in/chennai/show-times')
soup=BeautifulSoup(url.content,'lxml')
span_tag_2=soup.find_all('span',class_='filter-value movie__name')
list_1=[]
for onlystring in span_tag_2:
	a=onlystring.string
	list_1.append(a)	
span_tag_3=soup.find_all('span',class_='screen-name')
list_2=[]
for onlystring in span_tag_3:
	b=onlystring.string
	list_2.append(b)
span_tag_1=soup.find_all('span',class_='movie__language')
list_3=[]
for onlystring in span_tag_1:
	c=onlystring.string
	list_3.append(c)
span_tag_4=soup.find_all('span',class_='cinema-name')
list_4=[]
for onlystring in span_tag_4:
	d=onlystring.string
	list_4.append(d)
app=Flask(__name__)
@app.route('/spi_info')
def toJson1():
	arr=[]
	for index in range(0, len(list_2)):
		main_dict={}
		main_dict['Movie_name']=list_1[index]
		main_dict['Screen_Name']=list_2[index]
		main_dict['Movie_language']=list_3[index]
		main_dict['Theater_name']=list_4[index]
		arr.append(main_dict)
	return json.dumps(arr)


url_2=requests.get('https://www.spicinemas.in/chennai/now-showing')
soup_2=BeautifulSoup(url_2.content,'lxml')
all_links=soup_2.find_all('a',class_='filter-value')
arrr=[]
for link in all_links:
	l=link.get("href")
	arrr.append(l)
arr_1=[]
for i in arrr:
	url_1=requests.get('https://www.spicinemas.in'+i)
	soup_1=BeautifulSoup(url_1.content,'lxml')
	all_links_1=soup_1.find('div',attrs={'class' : 'movie__meta-data'}).findAll('a')
	for link_1 in all_links_1:
		l_1=link_1.get("href")
		arr_1.append(l_1)
def func():
	arr_3=[]
	for k in range(0,len(arrr)):
		dit={}
		dit['Movie_Link']=arrr[k]
		dit['MovieBuff_Link']=arr_1[k]
		arr_3.append(dit)
	return arr_3
@app.route('/spi_link')
def toJson2():
	g=func()
	return json.dumps(g)


response=requests.get('https://data.justickets.co/datapax/JUSTICKETS.chennai.v1.json')
data = response.json()
def func2():
	ls = []
	for js in data['movies']:
		di = {}
		di['id'] = js['id']
		di['name'] = js['name']
		di['language'] = js['language']
		di['moviebuff_url'] = js['moviebuff_url']
		di['theater_details']=js['urls']
		di['url']=js['url']
		di['certification']=js['certification']
		di['poster']=js['poster']
		di['synopsis']=js['synopsis']
		ls.append(di)
	return ls
@app.route('/justicket')
def toJson3():
	h=func2()
	return json.dumps(h)


if (__name__ == "__main__"):
	app.run(debug=True,port=7000)
